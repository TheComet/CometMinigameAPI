package co.thecomet.minigameapi.world;

import co.thecomet.core.utils.WorldCleaner;
import co.thecomet.minigameapi.config.BaseConfig;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

public class MapManager<T extends BaseConfig> implements Runnable {
    private Plugin plugin;
    private Class<T> configClass;
    private File directory;
    private boolean updating = true;
    private Map<String, File> allWorlds = new ConcurrentHashMap<>();
    private Map<String, File> unloadedWorlds = new ConcurrentHashMap<>();
    private NavigableMap<String, LoadedMap<T>> maps = new ConcurrentSkipListMap<>();
    private LoadedMap<T> current = null;
    private File currentCopy = null;

    public MapManager(Plugin plugin, Class<T> configClass) {
        this.plugin = plugin;
        this.configClass = configClass;
        
        File directory = new File(Bukkit.getWorldContainer(), "maps");
        if (directory.exists()) {
            if (!directory.isDirectory()) {
                directory.delete();
                directory.mkdir();
            }
        } else {
            directory.mkdir();
        }
        this.directory = directory;
        
        Bukkit.getScheduler().runTaskTimer(plugin, this, 20, 20 * 30);
    }
    
    @Override
    public void run() {
        if (updating) {
            for (File file : directory.listFiles()) {
                if (file.isDirectory()) {
                    boolean isWorld = false;
                    boolean isMap = false;
                    for (File f : file.listFiles()) {
                        if (f.getName().equalsIgnoreCase("level.dat")) {
                            isWorld = true;
                        }

                        if (f.getName().equalsIgnoreCase("map.json")) {
                            isMap = true;
                        }
                    }

                    if (isWorld) {
                        if (!allWorlds.containsKey(file.getName())) {
                            allWorlds.put(file.getName(), file);
                        }
                        
                        if (file.getName().equalsIgnoreCase(Bukkit.getWorlds().get(0).getName())) {
                            if (unloadedWorlds.containsKey(file.getName())) {
                                unloadedWorlds.remove(file.getName());
                            }

                            if (maps.containsKey(file.getName())) {
                                maps.remove(file.getName());
                            }

                            continue;
                        }
                        
                        if (isMap) {
                            if (!maps.containsKey(file.getName())) {
                                maps.put(file.getName(), new LoadedMap<T>(file, configClass));
                            }

                            if (unloadedWorlds.containsKey(file.getName())) {
                                unloadedWorlds.remove(file.getName());
                            }

                            continue;
                        }

                        if (!unloadedWorlds.containsKey(file.getName())) {
                            unloadedWorlds.put(file.getName(), file);
                        }
                    }
                }
            }
        }
    }

    public LoadedMap<T> cycle() throws MapNotFoundException, MapLoadException {
        if (maps.size() == 0) {
            throw new MapNotFoundException();
        }

        LoadedMap previous = current;
        if (maps.higherKey(previous.getWorld().getName()) == null) {
            current = maps.get(0);
        } else {
            current = maps.higherEntry(previous.getWorld().getName()).getValue();
        }

        MapSelectedEvent mapSelectedEvent = new MapSelectedEvent(current);
        Bukkit.getPluginManager().callEvent(mapSelectedEvent);

        if (mapSelectedEvent.getMap() != null) {
            current = mapSelectedEvent.getMap();
        }

         prepareWorld();
        
        if (currentCopy != null) {
            clean();
        }

        CycleCompleteEvent cycleCompleteEvent = new CycleCompleteEvent(current);
        Bukkit.getPluginManager().callEvent(cycleCompleteEvent);
        return cycleCompleteEvent.getMap();
    }

    public void loadSelectedMap(LoadedMap<T> map) throws MapLoadException {
        if (currentCopy != null) {
            clean();
        }

        current = map;
        prepareWorld();
    }

    public void prepareWorld() throws MapLoadException {
        clean();

        File copy = new File(Bukkit.getWorldContainer(), current.getWorld().getName());
        try {
            FileUtils.copyDirectory(current.getWorld(), copy);
        } catch (IOException e) {
            throw new MapLoadException();
        }
        
        currentCopy = copy;
        WorldCreator creator = new WorldCreator(copy.getName());
        Bukkit.getPluginManager().callEvent(new PreMapLoadEvent(current, creator));
        World world = Bukkit.createWorld(creator);
        world.setAutoSave(false);
        Bukkit.getPluginManager().callEvent(new PostMapLoadEvent(current, world));
    }

    public void clean() {
        if (currentCopy != null) {
            Bukkit.getWorlds().stream().filter(world -> world.getName().equalsIgnoreCase(currentCopy.getName())).forEach(world -> {
                WorldCleaner.unload(world, false);
            });

            if (currentCopy.exists()) {
                try {
                    FileUtils.deleteDirectory(currentCopy);
                } catch (IOException ignored) {}
            }
        }
    }
    
    public List<File> getUnloadedWorlds() {
        return new ArrayList<>(unloadedWorlds.values());
    }

    public List<LoadedMap<T>> getMaps() {
        return new ArrayList<>(maps.values());
    }

    public LoadedMap<T> getCurrent() {
        return current;
    }

    public void setCurrent(LoadedMap<T> current) {
        this.current = current;
    }

    public boolean isUpdating() {
        return updating;
    }

    public void setUpdating(boolean updating) {
        this.updating = updating;
    }

    public Map<String, File> getAllWorlds() {
        return new ConcurrentHashMap<>(allWorlds);
    }

    public File getCurrentCopy() {
        return currentCopy;
    }
}
