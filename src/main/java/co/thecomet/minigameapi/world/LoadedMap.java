package co.thecomet.minigameapi.world;

import co.thecomet.common.config.JsonConfig;
import co.thecomet.minigameapi.config.BaseConfig;

import java.io.File;

public class LoadedMap<T extends BaseConfig> implements GameMap {
    private File world;
    private Class<T> configClass;
    private T config;

    public LoadedMap(File world, Class<T> configClass) {
        this.world = world;
        this.configClass = configClass;
        this.loadConfig();
    }

    @Override
    public File getWorld() {
        return this.world;
    }

    @Override
    public String getMapName() {
        return this.config.mapName;
    }

    @Override
    public String getMapAuthor() {
        return this.config.mapAuthor;
    }

    public T getConfig() {
        return configClass.cast(config);
    }

    private void loadConfig() {
        this.config = JsonConfig.load(new File(world + File.separator + "map.json"), configClass);
    }
}
