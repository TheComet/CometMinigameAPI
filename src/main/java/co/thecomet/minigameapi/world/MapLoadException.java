package co.thecomet.minigameapi.world;

public class MapLoadException extends Exception {
    public MapLoadException() {
        super("Unable to load the current map!");
    }
}
