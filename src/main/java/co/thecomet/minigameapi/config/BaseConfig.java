package co.thecomet.minigameapi.config;

import co.thecomet.common.config.JsonConfig;

public class BaseConfig extends JsonConfig {
    public String mapName = "n/a";
    public String mapAuthor = "n/a";
}
